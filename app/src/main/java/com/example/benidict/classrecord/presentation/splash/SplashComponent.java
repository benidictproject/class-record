package com.example.benidict.classrecord.presentation.splash;

import com.example.benidict.classrecord.di.scope.PerActivity;

import dagger.Subcomponent;

/**
 * Created by benidict on 18/11/2017.
 */

@PerActivity
@Subcomponent(modules = SplashModule.class)
public interface SplashComponent {
    void inject(SplashActivity splashActivity);
}
