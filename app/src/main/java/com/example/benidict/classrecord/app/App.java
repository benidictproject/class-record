package com.example.benidict.classrecord.app;

import android.app.Application;

import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

/**
 * Created by benidict on 06/11/2017.
 */

public class App extends Application{


    @Inject
    FirebaseAuth auth;

    private AppComponent appComponent;


    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        appComponent.inject(this);

        auth = FirebaseAuth.getInstance();

    }

    public AppComponent getAppComponent(){
        return appComponent;
    }
}
