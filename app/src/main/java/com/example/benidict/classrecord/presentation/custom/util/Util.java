package com.example.benidict.classrecord.presentation.custom.util;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;



import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by benidict on 13/11/2017.
 */

public class Util {

    private String cellInfo;

    public String getFileName(Intent data, Context context){
        Uri uri = data.getData();
        String uriStr = uri.toString();
        File file = new File(uriStr);

        String displayName = null;

        if (uriStr.startsWith("content://")) {
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, null, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        } else if (uriStr.startsWith("file://")) {
            displayName = file.getName();

        }
        return displayName;
    }

//    public String readXLSXFile(String fileName){
//
//        try{
//            XSSFWorkbook workbook = new XSSFWorkbook();
//            XSSFSheet sheet = workbook.getSheetAt(0);
//            int rowsCount = sheet.getPhysicalNumberOfRows();
//            FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
//            for (int r = 0; r < rowsCount; r++){
//                Row row = sheet.getRow(r);
//                int cellsCount = row.getPhysicalNumberOfCells();
//                for (int c = 0 ;c < cellsCount; c++){
//                    String value = getCellAsString(row, c, formulaEvaluator);
//                    cellInfo = "row:"+r+"| cell:"+c+"| value:"+value;
//                }
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//
//        return cellInfo;
//    }
//
//    private String getCellAsString(Row row, int c, FormulaEvaluator formulaEvaluator){
//        String value = "";
//        try{
//            Cell cell = row.getCell(c);
//            CellValue cellValue = formulaEvaluator.evaluate(cell);
//            switch (cellValue.getCellType()){
//                case Cell.CELL_TYPE_BOOLEAN:
//                    value = ""+cellValue.getBooleanValue();
//                    break;
//                case Cell.CELL_TYPE_NUMERIC:
//                    double numericValue = cellValue.getNumberValue();
//                    if (HSSFDateUtil.isCellDateFormatted(cell)){
//                        double date = cellValue.getNumberValue();
//                        SimpleDateFormat formatter =
//                                new SimpleDateFormat("dd/MM/yy", Locale.ENGLISH);
//                        value = formatter.format(HSSFDateUtil.getJavaDate(date));
//                    }else{
//                        value = ""+numericValue;
//                    }
//                    break;
//                case Cell.CELL_TYPE_STRING:
//                    value = ""+cellValue.getStringValue();
//                    break;
//            }
//        }catch (NullPointerException e){
//            e.printStackTrace();
//        }
//
//        return value;
//    }

}
