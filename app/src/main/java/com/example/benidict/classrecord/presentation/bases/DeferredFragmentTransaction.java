package com.example.benidict.classrecord.presentation.bases;

import android.support.v4.app.Fragment;

/**
 * Created by benidict on 14/11/2017.
 */

public abstract class DeferredFragmentTransaction {
    private int contentFrameId;
    private Fragment replacingFragment;

    public abstract void commit();

    public int getContentFrameId(){
        return contentFrameId;
    }

    public void setContentFrameId(int contentFrameId){
        this.contentFrameId = contentFrameId;
    }

    public Fragment getReplacingFragment(){
        return replacingFragment;
    }

    public void setReplacingFragment(Fragment replacingFragment){
        this.replacingFragment = replacingFragment;
    }
}
