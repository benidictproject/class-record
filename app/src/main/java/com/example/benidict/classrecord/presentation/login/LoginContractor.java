package com.example.benidict.classrecord.presentation.login;

import android.widget.EditText;

/**
 * Created by benidict on 11/11/2017.
 */

@SuppressWarnings("unused")
public interface LoginContractor {

    interface View{
            void showSuccessful(String successMessage);
            void showError(String error);
            void goNavigate(String email);
    }

    interface Presenter{
            void validateFields(EditText email, EditText pass);
            void logIn(String email, String pass);
    }

}
