package com.example.benidict.classrecord.presentation.home;



import com.example.benidict.classrecord.R;
import com.example.benidict.classrecord.presentation.bases.BaseFragment;

/**
 * Created by benidict on 02/12/2017.
 */

public class FragmentHome extends BaseFragment{

    @Override
    protected int getFragmentLayout() {
        return R.layout.home_fragment;
    }
}
