package com.example.benidict.classrecord.app;



import android.app.Application;
import android.content.Context;

import com.example.benidict.classrecord.data.NetworkModule;
import com.example.benidict.classrecord.presentation.bases.BaseComponents;
import com.example.benidict.classrecord.presentation.custom.util.UtilModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by benidict on 06/11/2017.
 */

@Singleton
@SuppressWarnings("WeakerAccess")
@Component(modules = {AppModule.class, NetworkModule.class, UtilModule.class})
public interface AppComponent extends BaseComponents {
    Application application();
    Context context();
    void inject(App app);
}
