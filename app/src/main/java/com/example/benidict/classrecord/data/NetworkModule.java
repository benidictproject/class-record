package com.example.benidict.classrecord.data;




import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


/**
 * Created by benidict on 10/11/2017.
 */

@Module
public class NetworkModule {

    @Provides
    @Singleton
    FirebaseAuth provideFirebaseAuth(){
       return FirebaseAuth.getInstance();
    }

}
