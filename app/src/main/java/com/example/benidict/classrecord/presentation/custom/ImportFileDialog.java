package com.example.benidict.classrecord.presentation.custom;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.benidict.classrecord.R;
import com.example.benidict.classrecord.presentation.custom.callback.CallBackImportFile;
import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;



/**
 * Created by benidict on 12/11/2017.
 */

@SuppressWarnings("WeakerAccess")
public class ImportFileDialog implements View.OnClickListener{

    private Context context;
    public Dialog dialog;
    public TextView tvMessage;
    public Button btnChoose, btnImport;
    private CallBackImportFile callBackImportFile;
    public View import_loader, import_main_layout;
    private boolean isChooseFuncBool = false;
    public static final int TIMER_VALUE = 6;


    public ImportFileDialog(Context context, CallBackImportFile callBackImportFile){
        this.context = context;
        this.callBackImportFile = callBackImportFile;
    }

    public boolean isChooseFunc(boolean isChoose){
        isChooseFuncBool = isChoose;
        return isChooseFuncBool;
    }

    public void createDialog(){
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.import_layout);

        tvMessage = dialog.findViewById(R.id.text_dialog);
        btnChoose = dialog.findViewById(R.id.btn_choose);
        btnChoose.setOnClickListener(this);
        btnImport = dialog.findViewById(R.id.btn_import);
        btnImport.setOnClickListener(this);
        import_main_layout = dialog.findViewById(R.id.import_main_layout);
        import_loader = dialog.findViewById(R.id.import_loader);


        dialog.show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_choose:
                    callBackImportFile.onChooseListener();
                break;
            case R.id.btn_import:
                if (isChooseFuncBool){

                    Observable.interval(1, TimeUnit.SECONDS)
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnNext(aLong -> {
                                import_main_layout.setVisibility(View.GONE);
                                import_loader.setVisibility(View.VISIBLE);
                            })
                            .doOnError(throwable -> {
                                callBackImportFile.onImportListener();
                            })
                            .takeUntil(aLong -> aLong == TIMER_VALUE)
                            .doOnCompleted(()-> {
                                callBackImportFile.onImportListener();
                            })
                            .subscribe();
                }else{
                    Toast.makeText(context, "Choose File!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


}
