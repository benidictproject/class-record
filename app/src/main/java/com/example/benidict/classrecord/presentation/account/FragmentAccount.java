package com.example.benidict.classrecord.presentation.account;

import com.example.benidict.classrecord.R;
import com.example.benidict.classrecord.presentation.bases.BaseFragment;

/**
 * Created by benidict on 19/11/2017.
 */

public class FragmentAccount extends BaseFragment{

    @Override
    protected int getFragmentLayout() {
        return R.layout.account_fragment;
    }


}
