package com.example.benidict.classrecord.presentation.splash;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by benidict on 18/11/2017.
 */

@SuppressWarnings("WeakerAccess")
public class SplashScreenPresenter implements SplashScreenContractor.Presenter{

    private SplashScreenContractor.View mView;
    private FirebaseAuth auth;

    public SplashScreenPresenter(SplashScreenContractor.View view, FirebaseAuth auth){
        this.mView = view;
        this.auth = auth;
    }

    public void isUserLoggedIn() {
        if (auth.getCurrentUser() != null){
            mView.showParentPage();
        }else{
            mView.showLoginPage();
        }

    }
}
