package com.example.benidict.classrecord.presentation.custom.module;

import android.content.Context;

import com.example.benidict.classrecord.di.scope.PerActivity;
import com.example.benidict.classrecord.presentation.custom.ImportFileDialog;
import com.example.benidict.classrecord.presentation.custom.callback.CallBackImportFile;

import dagger.Module;
import dagger.Provides;

/**
 * Created by benidict on 12/11/2017.
 */

@Module
public class CustomDialogModule {

        private Context context;
        private CallBackImportFile callBackImportFile;

        public CustomDialogModule(Context context, CallBackImportFile callBackImportFile){
            this.context = context;
            this.callBackImportFile = callBackImportFile;
        }

    @Provides
    @PerActivity
    CallBackImportFile provideCallBackImportFile(){
            return callBackImportFile;
    }

    @Provides
    @PerActivity
    ImportFileDialog provideImportFileDialog(){
           return new ImportFileDialog(context, callBackImportFile);
    }
}
