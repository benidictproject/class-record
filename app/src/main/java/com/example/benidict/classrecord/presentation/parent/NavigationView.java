package com.example.benidict.classrecord.presentation.parent;

/**
 * Created by benidict on 14/11/2017.
 */
public interface NavigationView {

        int SCREEN_HOME = -1;
        int SCREEN_ACCOUNT = 2;
        int SCREEN_IMPORT = 3;

        void changeFragment(int screen);

}
