package com.example.benidict.classrecord.presentation.bases;

import com.example.benidict.classrecord.presentation.parent.ParentComponent;
import com.example.benidict.classrecord.presentation.custom.module.CustomDialogModule;
import com.example.benidict.classrecord.presentation.login.LoginComponent;
import com.example.benidict.classrecord.presentation.login.LoginModule;
import com.example.benidict.classrecord.presentation.splash.SplashComponent;
import com.example.benidict.classrecord.presentation.splash.SplashModule;

/**
 * Created by benidict on 06/11/2017.
 */

public interface BaseComponents {
    SplashComponent plus(SplashModule splashModule);
    LoginComponent plus(LoginModule loginModule);
    ParentComponent plus(CustomDialogModule customDialogModule);
}
