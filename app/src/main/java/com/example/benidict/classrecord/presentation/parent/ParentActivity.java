package com.example.benidict.classrecord.presentation.parent;


import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.example.benidict.classrecord.Constant;
import com.example.benidict.classrecord.R;
import com.example.benidict.classrecord.app.App;
import com.example.benidict.classrecord.presentation.NavigationUtil;
import com.example.benidict.classrecord.presentation.account.FragmentAccount;
import com.example.benidict.classrecord.presentation.bases.BaseDrawerActivity;
import com.example.benidict.classrecord.presentation.custom.ImportFileDialog;
import com.example.benidict.classrecord.presentation.custom.callback.CallBackImportFile;
import com.example.benidict.classrecord.presentation.custom.module.CustomDialogModule;
import com.example.benidict.classrecord.presentation.custom.util.Util;
import com.example.benidict.classrecord.presentation.fragment.FragmentImport;
import com.example.benidict.classrecord.presentation.home.FragmentHome;
import com.example.benidict.classrecord.presentation.login.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

import butterknife.BindView;



/**
 * Created by benidict on 11/11/2017.
 */

public class ParentActivity extends BaseDrawerActivity implements
        CallBackImportFile, NavigationView{

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    ImportFileDialog importFileDialog;

    @Inject
    Util util;

    @Inject
    FirebaseAuth auth;

    private int lastSelectedMenuItemId;
    private Fragment changedFragment = null;
    private boolean isRunning;



    @Override
    protected int getResLayout() {
        return R.layout.parent_activity;
    }

    @Override
    protected int getNavigationViewLayout() {
        return R.layout.layout_navigation;
    }

    @Override
    public void initView() {
        super.initView();
        initDaggerComponent();
        setToolbarVisibility(1);
        changeFragment(-1);
    }

    @Override
    public void initDaggerComponent() {
        ((App)getApplicationContext()).getAppComponent()
                .plus(new CustomDialogModule(this, this))
                .inject(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (lastSelectedMenuItemId == 0){
            lastSelectedMenuItemId = R.id.menu_1;
        }
        setActionView(lastSelectedMenuItemId, View.VISIBLE);
    }

    public void setDrawerEnabled(boolean enabled) {
        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        getNewDrawerLayout().setDrawerLockMode(lockMode);
    }

    public void setToolbarVisibility(int visibility){
        if (visibility == 0){
            toolbar.setVisibility(View.GONE);
        }else{
            toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            toolbar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDrawerItemSelected(int itemId) {
        switch (itemId) {
            case R.id.menu_1:
                //You only have access on drawer at home screen, consider deleting this item
        /*        navigateTo(SCREEN_HOME);*/
                switchSelectedMenuItem(itemId);
                changeFragment(SCREEN_ACCOUNT);
                break;
            case R.id.menu_2:

                break;
            case R.id.menu_3:
                break;
            case R.id.menu_4:
                break;
            case R.id.menu_5:
                break;
            case R.id.menu_import_files:
                importFileDialog.createDialog();
                break;
            case R.id.menu_logout:
                signOut();
                break;

            default:
                break;
        }
    }

    @Override
    public void onChooseListener() {
        startActivityForResult(NavigationUtil.startFileIntent(), 7);
    }

    @Override
    public void onImportListener() {

            if (isRunning){
                importFileDialog.dialog.dismiss();
                changeFragment(SCREEN_IMPORT);
            }else{
                importFileDialog.dialog.dismiss();
                replaceFragmentAddToBackStack(R.id.fragment_container, new FragmentImport());
               // replaceFragmentAllowingStateLoss(R.id.fragment_container, new FragmentImport());
            }
    }

    @Override
    public void changeFragment(int screen) {
        switch (screen){
            case SCREEN_HOME:
                changedFragment = new FragmentHome();
                break;
            case SCREEN_IMPORT:
                changedFragment = new FragmentImport();
                break;
            case SCREEN_ACCOUNT:
                changedFragment = new FragmentAccount();
                break;
        }
        Constant.fragmentPosition = screen;
        if (changedFragment != null){


//            Toast.makeText(getApplicationContext(), "checkfragment: " + currentFragment + " changedFragment: " + changedFragment,
//                    Toast.LENGTH_SHORT).show();
//            if (changedFragment.getClass().equals())

            replaceFragment(R.id.fragment_container, changedFragment);
        }
    }


    public void switchSelectedMenuItem(int itemId){
        setActionView(lastSelectedMenuItemId, View.INVISIBLE);
        lastSelectedMenuItemId = itemId;
        setActionView(itemId, View.VISIBLE);
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void initToolbar(Toolbar toolbar) {
        super.initToolbar(toolbar);
        if (null != getSupportActionBar()){
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        if (TextUtils.isEmpty(title)){
            //title gone
        }else{
            //title set and visible
        }
    }

    @Override
    protected void onClickNavigationIcon() {
        super.onClickNavigationIcon();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 7:
                if (resultCode == RESULT_OK) {
                    importFileDialog.isChooseFunc(true);
                    importFileDialog.tvMessage.setText(util.getFileName(data, this));

                }
                break;
        }
    }

    @Override
    public void replaceFragmentAddToBackStack(int contentFrameId, Fragment replacingFragment) {
        super.replaceFragmentAddToBackStack(contentFrameId, replacingFragment);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isRunning = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isRunning = false;
    }

    private void signOut(){
        auth.signOut();
        startActivity(new Intent(ParentActivity.this, LoginActivity.class));
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    public void onBackPressed() {
        switch (Constant.fragmentPosition){
            case NavigationView.SCREEN_ACCOUNT:
                changeFragment(NavigationView.SCREEN_HOME);
                break;
                default:
                    super.onBackPressed();
                    break;
        }
    }

}
