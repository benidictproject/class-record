package com.example.benidict.classrecord.presentation.custom.callback;

/**
 * Created by benidict on 13/11/2017.
 */

public interface CallBackImportFile {
    void onChooseListener();
    void onImportListener();
}
