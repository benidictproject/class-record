package com.example.benidict.classrecord.presentation.bases;

import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.benidict.classrecord.R;

/**
 * Created by benidict on 11/11/2017.
 */

public abstract class BaseDrawerActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener,
        DrawerLayout.DrawerListener{

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private boolean isNavigationItemClicked;
    private int itemId;

    @LayoutRes
    protected abstract int getNavigationViewLayout();
    protected abstract void onDrawerItemSelected(int itemId);

    @LayoutRes
    private int getDrawerLayout(){
        return R.layout.drawer_layout;
    }

    @IdRes
    private int getDrawerLayoutId(){
        return R.id.drawer_layout;
    }

    @Override
    protected int getNavigationIconResources() {
        return R.drawable.ic_menu;
    }


    @Override
    protected void setContentView() {
        setContentView(getDrawerLayout());
        ViewGroup viewGroup = findViewById(android.R.id.content);
        if (null != viewGroup){
            ViewGroup view = (ViewGroup) viewGroup.getChildAt(0 );
            LayoutInflater inflater = LayoutInflater.from(this);
            view.addView(inflater.inflate(getResLayout(), view, false), 0 );
        }
    }

    @Override
    public void initView() {
        super.initView();
        drawerLayout = findViewById(getDrawerLayoutId());
        if (getNavigationViewLayout() == 0){
            throw  new IllegalArgumentException("Navigation View Layout cannot be null");
        }
        navigationView = (NavigationView) LayoutInflater.from(this)
                .inflate(getNavigationViewLayout(), drawerLayout, false);

        drawerLayout.addView(navigationView);
        drawerLayout.addDrawerListener(this);
        if (null != navigationView){
            navigationView.setNavigationItemSelectedListener(this);
        }
    }

    @Override
    protected void onClickNavigationIcon() {
        toggleDrawer();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        isNavigationItemClicked = true;
        itemId = item.getItemId();

        drawerLayout.closeDrawers();
        return false;
    }

    protected int getDrawerGravity(){
        return GravityCompat.START;
    }

    @SuppressWarnings("WrongConstant")
    protected boolean isDrawerOpen(){
        return null != drawerLayout &&
                drawerLayout.isDrawerOpen(getDrawerGravity());
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        if (isNavigationItemClicked){
            onDrawerItemSelected(itemId);
        }
        isNavigationItemClicked = false;
    }



    @SuppressWarnings("WrongConstant")
    protected void toggleDrawer(){
        if (!drawerLayout.isDrawerOpen(getDrawerGravity())){
            drawerLayout.openDrawer(getDrawerGravity());
        }
    }

    public void setActionView(@IdRes int id, int visibility){
         navigationView.getMenu().findItem(id).getActionView().setVisibility(visibility);
    }

    public DrawerLayout getNewDrawerLayout(){
        return drawerLayout;
    }

    @SuppressWarnings("WrongConstant")
    @Override
    public void onBackPressed() {
        if (isDrawerOpen()){
            drawerLayout.closeDrawer(getDrawerGravity());
        }else{
            super.onBackPressed();
        }
    }

    /** Required **/
    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }


    @Override
    public void onDrawerStateChanged(int newState) {

    }


}
