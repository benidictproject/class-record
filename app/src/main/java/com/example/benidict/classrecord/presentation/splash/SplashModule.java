package com.example.benidict.classrecord.presentation.splash;

import com.example.benidict.classrecord.di.scope.PerActivity;
import com.google.firebase.auth.FirebaseAuth;

import dagger.Module;
import dagger.Provides;

/**
 * Created by benidict on 18/11/2017.
 */

@Module
public class SplashModule {

    private SplashScreenContractor.View mView;

    public SplashModule(SplashScreenContractor.View view){
        this.mView = view;
    }

    @Provides
    @PerActivity
    SplashScreenContractor.View provideSplashScreenContractorView(){
        return mView;
    }

    @Provides
    @PerActivity
    SplashScreenPresenter provideSplashScreenPresenter(FirebaseAuth auth){
        return new SplashScreenPresenter(mView,auth);
    }
}
