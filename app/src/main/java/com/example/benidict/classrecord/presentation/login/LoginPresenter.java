package com.example.benidict.classrecord.presentation.login;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by benidict on 11/11/2017.
 */

@SuppressWarnings("WeakerAccess")
public class LoginPresenter implements LoginContractor.Presenter{

    LoginContractor.View mView;
    String Strpass, Stremail;
    Context context;
    FirebaseAuth auth;
    Activity activity;

    public LoginPresenter(LoginContractor.View view, Context context, FirebaseAuth auth, Activity activity){
        this.mView = view;
        this.context = context;
        this.auth = auth;
        this.activity = activity;
    }



    @Override
    public void validateFields(EditText email, EditText pass) {
                if (email.getText().toString().isEmpty() || pass.getText().toString().isEmpty()){
                    email.setError("Provide the required field!");
                    mView.showError("Provide the required field!");
                }else{
                    validated(email, pass);
                }
    }

    private void validated(EditText email, EditText pass){
        Stremail = email.getText().toString();
        Strpass = pass.getText().toString();

        checkCredentials(Stremail,Strpass);
    }

    private void checkCredentials(String email, String pass){
        logIn(email, pass);
    }

    @Override
    public void logIn(String email, String pass) {

        auth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()){
                            mView.showError("Wrong Username or Password");
                        }else{
                            mView.showSuccessful("Success!");
                            mView.goNavigate("adsa");
                        }
                    }
                });
    }
}
