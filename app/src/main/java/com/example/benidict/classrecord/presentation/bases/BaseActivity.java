package com.example.benidict.classrecord.presentation.bases;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;


import com.example.benidict.classrecord.R;

import java.util.ArrayDeque;
import java.util.Queue;

import butterknife.ButterKnife;

import static android.graphics.Color.WHITE;


/**
 * Created by benidict on 06/11/2017.
 */

public abstract class BaseActivity extends AppCompatActivity{

    private Toolbar toolbar;
    private boolean isBaseActivityRunning;
    Queue<DeferredFragmentTransaction> deferredFragmentTransactions = new ArrayDeque<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView();
        ButterKnife.bind(this);
        initDaggerComponent();
        initView();
        initToolbar(getToolbar());
    }

    protected abstract int getResLayout();
    protected void setContentView(){
        setContentView(getResLayout());
    }
    public void initDaggerComponent(){}
    public void initView(){}

    @DrawableRes
    protected int getNavigationIconResources(){
        return R.drawable.ic_arrow;
    }

    protected void initToolbar(Toolbar toolbar){
        if (toolbar == null){
            return;
        }

        Drawable drawable = ContextCompat.getDrawable(this, getNavigationIconResources());
            drawable.setColorFilter(Color.parseColor("#f8be15"), PorterDuff.Mode.SRC_IN);
        toolbar.setNavigationIcon(drawable);
        toolbar.setTitle(getTitle());
        this.toolbar = toolbar;
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onClickNavigationIcon();
                break;
                default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public Toolbar getToolbar(){
        return toolbar;
    }

    protected void onClickNavigationIcon(){
        onBackPressed();
    }

//    @Override
//    public void onBackPressed() {
//        Toast.makeText(getApplicationContext(), "backStackEntryCount: " + getSupportFragmentManager().getBackStackEntryCount()
//                , Toast.LENGTH_SHORT).show();
//
//        if (getSupportFragmentManager().getBackStackEntryCount() > 0){
//            getSupportFragmentManager().popBackStack();
//        }else{
//            super.onBackPressed();
//        }
//    }

    protected void setNavigationIcon(@DrawableRes int drawableRes){
        Drawable drawable = ContextCompat.getDrawable(this, drawableRes);
        drawable.setColorFilter(WHITE, PorterDuff.Mode.SRC_IN);
        toolbar.setNavigationIcon(drawable);
    }

    public void addFragment(int containerId, Fragment fragment){
        FragmentTransaction transaction = this.getSupportFragmentManager()
                .beginTransaction();
        transaction.add(containerId, fragment);
        transaction.commit();
    }

    public void replaceFragment(int containerId, Fragment fragment){
//        Fragment currentFragment = getSupportFragmentManager().findFragmentById(containerId);
        FragmentTransaction transaction = this.getSupportFragmentManager()
                .beginTransaction();
       // transaction.addToBackStack(null);
        transaction.replace(containerId, fragment);
        transaction.commit();
    }

    public void replaceFragmentAllowingStateLoss(int containerId, Fragment fragment){
        FragmentTransaction transaction = this.getSupportFragmentManager()
                .beginTransaction();
        transaction.replace(containerId, fragment);
        transaction.commitAllowingStateLoss();
    }

    public void replaceFragmentAddToBackStack(int contentFrameId, Fragment replacingFragment){
        if (isBaseActivityRunning){
            //This will handle switching of fragments when the activity is paused. To prevent IllegalSTateExecption.
            //This transaction will be used in the resume part too.
            DeferredFragmentTransaction deferredFragmentTransaction = new DeferredFragmentTransaction() {
                @Override
                public void commit() {
                    replaceFragmentAddToBackStackInternal(getContentFrameId(), getReplacingFragment());
                }
            };

            deferredFragmentTransaction.setContentFrameId(contentFrameId);
            deferredFragmentTransaction.setReplacingFragment(replacingFragment);
            deferredFragmentTransactions.add(deferredFragmentTransaction);
        }else{
            replaceFragmentAddToBackStackInternal(contentFrameId, replacingFragment);
        }
    }

    private void replaceFragmentAddToBackStackInternal(int contentFrameId, Fragment replacingFragment) {

        FragmentTransaction fragmentManager = this.getSupportFragmentManager()
                .beginTransaction();
                fragmentManager.replace(contentFrameId, replacingFragment);
                fragmentManager.addToBackStack(replacingFragment.getClass().getSimpleName());
                fragmentManager.commitAllowingStateLoss();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        while (!deferredFragmentTransactions.isEmpty()){
            deferredFragmentTransactions.remove().commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isBaseActivityRunning = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isBaseActivityRunning = false;
    }

    public void showToastMessage(String message){
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

}
