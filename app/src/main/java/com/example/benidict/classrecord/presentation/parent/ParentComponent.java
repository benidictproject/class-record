package com.example.benidict.classrecord.presentation.parent;

import com.example.benidict.classrecord.di.scope.PerActivity;
import com.example.benidict.classrecord.presentation.custom.module.CustomDialogModule;

import dagger.Subcomponent;

/**
 * Created by benidict on 12/11/2017.
 */

@PerActivity
@Subcomponent(modules = {CustomDialogModule.class})
public interface ParentComponent {
    void inject(ParentActivity parentActivity);
}
