package com.example.benidict.classrecord.app;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by benidict on 06/11/2017.
 */

@Module
@SuppressWarnings("WeakerAccess")
public class AppModule {

    private Application application;

    public AppModule(Application application){
        this.application = application;
    }

    @Provides
    @Singleton
    Application provideApplication(){
        return application;
    }

    @Provides
    @Singleton
    Context provideContext(){
        return application;
    }
}
