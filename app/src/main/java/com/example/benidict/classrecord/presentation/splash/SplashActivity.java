package com.example.benidict.classrecord.presentation.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.benidict.classrecord.R;
import com.example.benidict.classrecord.app.App;
import com.example.benidict.classrecord.presentation.bases.BaseActivity;
import com.example.benidict.classrecord.presentation.login.LoginActivity;
import com.example.benidict.classrecord.presentation.parent.ParentActivity;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by benidict on 06/11/2017.
 */

public class SplashActivity extends BaseActivity implements SplashScreenContractor.View{

    @Inject
    SplashScreenPresenter presenter;

    @Override
    protected int getResLayout() {
        return R.layout.splash_activity;
    }

    @Override
    public void initDaggerComponent() {
        ((App)getApplicationContext()).getAppComponent().plus(new SplashModule(this))
                .inject(this);
    }

    @Override
    public void initView() {
        Observable.timer(3,
                TimeUnit.SECONDS)
                .doOnCompleted(this::launchActivity)
                .subscribe();
    }

    private void launchActivity(){
        presenter.isUserLoggedIn();
    }

    @Override
    public void showLoginPage() {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
    }

    @Override
    public void showParentPage() {
        startActivity(new Intent(getApplicationContext(), ParentActivity.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
