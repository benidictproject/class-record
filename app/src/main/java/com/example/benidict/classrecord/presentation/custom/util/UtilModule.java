package com.example.benidict.classrecord.presentation.custom.util;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


/**
 * Created by benidict on 13/11/2017.
 */

@Module
public class UtilModule {

    @Provides
    @Singleton
    Util provideUtil(){
        return new Util();
    }

}
