package com.example.benidict.classrecord.presentation;

import android.content.Context;
import android.content.Intent;

/**
 * Created by benidict on 12/11/2017.
 */

@SuppressWarnings("WeakerAccess")
public class NavigationUtil {


    public static void startChooseFile(Context context){
//        startActivity(context, );
    }

    public static Intent startFileIntent(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        return intent;
    }

    public static void startActivity(Context context, Intent intent) {
        context.startActivity(intent);
    }

}
