package com.example.benidict.classrecord.presentation.login;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.benidict.classrecord.R;
import com.example.benidict.classrecord.app.App;
import com.example.benidict.classrecord.presentation.parent.ParentActivity;
import com.example.benidict.classrecord.presentation.bases.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by benidict on 11/11/2017.
 */

public class LoginActivity extends BaseActivity implements LoginContractor.View{

    @BindView(R.id.et_password)
    EditText et_password;

    @BindView(R.id.et_email)
    EditText et_email;

    @BindView(R.id.btn_submit)
    Button btn_submit;

    @BindView(R.id.loader)
    ProgressBar loader;

    @Inject LoginPresenter presenter;

    @Override
    protected int getResLayout() {
        return R.layout.login_activity;
    }

    @Override
    public void initDaggerComponent() {
        ((App)getApplicationContext()).getAppComponent()
                .plus(new LoginModule(this, this))
                .inject(this);
    }

    @Override
    public void initView() {

    }

    @OnClick(R.id.btn_submit)
    public void clickedSubmit(View view){
        visibility(0,4,4,4);
            presenter.validateFields(et_email, et_password);
    }

    @Override
    public void showError(String error) {
        visibility(8,0,0,0);
        showToastMessage(error);
    }

    @Override
    public void showSuccessful(String successMessage) {
        showToastMessage(successMessage);
    }

    @Override
    public void goNavigate(String email) {
        visibility(8,4,4,4);
        startActivity(new Intent(this, ParentActivity.class));
    }

    @Override
    public void showToastMessage(String message) {
        super.showToastMessage(message);
    }

    public void visibility(int loaderer, int etEmail, int etPass, int btnSubmit){
        loader.setVisibility(loaderer);
        et_email.setVisibility(etEmail);
        et_password.setVisibility(etPass);
        btn_submit.setVisibility(btnSubmit);
    }
}
