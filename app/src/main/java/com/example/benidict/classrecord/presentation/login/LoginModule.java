package com.example.benidict.classrecord.presentation.login;

import android.app.Activity;
import android.content.Context;

import com.example.benidict.classrecord.di.scope.PerActivity;
import com.google.firebase.auth.FirebaseAuth;

import dagger.Module;
import dagger.Provides;

/**
 * Created by benidict on 11/11/2017.
 */

@Module
@SuppressWarnings("WeakerAccess")
public class LoginModule {

    private LoginContractor.View mView;
    Activity activity;

    public LoginModule(LoginContractor.View view, Activity activity){
        this.mView = view;
        this.activity = activity;
    }

    @PerActivity
    @Provides
    LoginContractor.View provideLoginContractorView(){
        return mView;
    }

    @PerActivity
    @Provides
    LoginPresenter provideLoginPresenter(Context context, FirebaseAuth auth){
        return new LoginPresenter(mView, context, auth, activity);
    }
}
