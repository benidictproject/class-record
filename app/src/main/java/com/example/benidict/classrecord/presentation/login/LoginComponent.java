package com.example.benidict.classrecord.presentation.login;

import com.example.benidict.classrecord.di.scope.PerActivity;

import dagger.Subcomponent;

/**
 * Created by benidict on 11/11/2017.
 */

@PerActivity
@Subcomponent(modules = {LoginModule.class})
public interface LoginComponent {
    void inject(LoginActivity loginActivity);
}
