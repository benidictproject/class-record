package com.example.benidict.classrecord.presentation.splash;

/**
 * Created by benidict on 18/11/2017.
 */

public interface SplashScreenContractor {
    interface View{
        void showLoginPage();
        void showParentPage();
    }

    interface Presenter{
        void isUserLoggedIn();
    }
}
